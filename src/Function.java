
/**
 * 
 *	A stupid attempt at implementing function programming in Java.
 *	Use at your own discretion. <br>
 *	<br>
 *	
 *	Simple usecase:<br>
 *	&nbsp&nbsp&nbsp Function a = new Function&lt Integer &gt() {<br>
 *	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp @Override<br>
	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp public Integer call(Object... args) {<br>
	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp // args is an array of type Object. Literary Anything<br>
	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp return new Integer(1);<br>
				 
	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp }<br>
 
 *	&nbsp&nbsp&nbsp <br>
 *	&nbsp&nbsp&nbsp }<br>
 * @param <T> the return type of method <i>call</i>
 */
public interface Function<T > {

	 /**
	  *	This is the function you should call 
	 * @return You define!
	 */
	public T call(Object... args);
 
}

/**
 * A class to use instead of <b>void</b> when you don't want to have a useful return statement.
 *
 */
 class Void{
	public String toString(){
		return "Nothing";
	}
};


final class FunctionT implements Runnable{
	
	private Function func = null;
	private boolean done = false;
	private Object returnValue = null;
	private Object[] arguments = null;
	private boolean destroyFunction = false;
	public FunctionT(Function f, Object... args){
		func = f;
		arguments = args;
		new Thread(this).start();
	}
	public FunctionT(boolean destroy, Function f, Object... args ){
		func = f;
		arguments = args;
		destroyFunction = destroy;
		new Thread(this).start();
	}
	@Override
	public void run() {
		 
		done = false;
		returnValue = (func.call(arguments));
		if(destroyFunction){
			func = null; 
			arguments = null;
		}
		done = true;
	}
	public boolean isDone() {
		return done;
	}
	 
	public Object getReturnValue() {
		return returnValue;
	}
	public Function getFunction(){
		return func;
	}
	public Object[] getArguments(){
		return arguments;
	}
 
	public static void waitUntilDone( FunctionT a) throws InterruptedException{
		while(!a.done){ Thread.sleep(100);};
	}
	
	public static void waitUntilDone( FunctionT a, long milis) throws InterruptedException{
		while(!a.done){ Thread.sleep(milis);};
	}
	
}