import java.sql.Connection;
public class TestClass {
	
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Function a = new Function<Void>() {
			StringBuffer printable = new StringBuffer("");
			@Override
			public Void call(Object... a) {
				printable.setLength(0);
			    for (Object baz : a) {
			    	printable.append( baz.toString());
			    	printable.append(" ");
			    }
				System.out.println(printable);
				return null;
			}
		};
		 
		a.call("HELLO!");
		 
		a.call("HELLO!", new Integer(1), 1, 0.2, 'a');
		FunctionT b = new FunctionT(SomeFuncDefs.readFile,  "TestFile001") ;
		System.out.println(b.getReturnValue());
		new FunctionT(a, "HELLO!", new Integer(1), 1, 0.2, 'a', 'h', 'e' , "llo");
		new FunctionT(a,   0.2, 'a');
		try {
			FunctionT.waitUntilDone(b);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(b.getReturnValue());
		
	}

}
