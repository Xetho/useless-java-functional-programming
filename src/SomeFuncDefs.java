import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.CharBuffer;

public class SomeFuncDefs {
	public static Function readFile = new Function<StringBuffer>() {


		@Override
		public StringBuffer call(Object... args) {
			
			StringBuffer result = null;
			BufferedReader br = null;
			try {
				
				  br =  new BufferedReader(new FileReader(new File((String) args[0])));
				result = new StringBuffer();
				while(br.ready()){
					result.append(br.readLine()+"\n");
				}
				
			} catch (FileNotFoundException e) {
				 e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
			 
		}
		
		
	};
	public static Function readFileT = new Function<Void>(){

		@Override
		public Void call(Object... args) {
			
			FunctionT b = new FunctionT(SomeFuncDefs.readFile,  args[0]) ;
			try {
				FunctionT.waitUntilDone(b);
				 ((Function) args[1]).call(b.getReturnValue());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
	};
}
